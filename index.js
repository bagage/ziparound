const prompt = require('prompt')
const optimist = require('optimist')
const haversine = require('haversine')
const util = require('util')
const fetch = require('node-fetch')
require('dotenv').config()

let places;
try {
  places = require('./laposte.json');
} catch {
  console.error('Missing laposte.json file. Please download it first!');
  return 1;
}
const mapboxApi = 'https://api.mapbox.com/directions/v5/mapbox/driving/'

const mapboxToken = process.env.MAPBOX_ACCESS_TOKEN

const schema = {
  properties: {
    insee: {
      type: 'boolean',
      description: 'Use INSEE code search? (otherwise ZIP)',
      default: true,
      required: true,
    },
    code: {
      type: 'integer',
      description: 'Code of search origin',
      message: 'Integer, example: 75001',
      required: true,
    },
    distance: {
      type: 'integer',
      description: 'Distance to search around, in km',
      message: 'Integer, example: 25',
      default: 5,
      required: true,
    },
  },
};
if (mapboxToken) {
  schema.properties.duration = {
    type: "integer",
    description: "Travel time in car, in minutes",
    message: "Integer, example: 30",
    required: true
  }
}
prompt.message = ''
prompt.override = optimist.argv
prompt.start()
prompt.get(schema, async (err, input) => {
  if (err) {
    console.log(err);
    return;
  }
  const use_zip = input.insee === false;
  const origin = places.find(
    (x) =>
      (use_zip ? x.fields.code_postal : x.fields.code_commune_insee) ===
      input.code.toString()
  );
  if (origin) {
    const reduction1 = reduceAsCrowFiles(input, origin)
    let reduction2
    if (mapboxToken) {
      reduction2 = await reduceOnTravelDuration(input, origin, reduction1)
    }
    // Results.
    const results = reduction2 ? reduction2 : reduction1
    // Eliminate duplicates, mostly if only simple reduction was applied.
    const uniqueCodes = [...new Set(results)].sort((a,b) => a.code - b.code);
    console.log(uniqueCodes.length + ' results found:');
    console.log(util.inspect(uniqueCodes, { maxArrayLength: null }));
  }
  else {
    console.error('ZIP of search origin not in database, sorry.')
  }
})

function reduceAsCrowFiles(input, origin, use_zip) {
  const start = {
    latitude: origin.fields.coordonnees_gps[0],
    longitude: origin.fields.coordonnees_gps[1],
  };
  return places.reduce((result, place) => {
    if (place.fields.coordonnees_gps) {
      const end = {
        latitude: place.fields.coordonnees_gps[0],
        longitude: place.fields.coordonnees_gps[1],
      };
      const d = haversine(start, end);
      if (d <= input.distance) {
        result.push({
          code: use_zip ? place.fields.code_postal : place.fields.code_commune_insee,
          latitude: place.fields.coordonnees_gps[0],
          longitude: place.fields.coordonnees_gps[1],
        });;
      }
    }
    return result;
  }, []);
}

async function reduceOnTravelDuration(input, origin, reduction1) {
  const start = {
    latitude: origin.fields.coordonnees_gps[0],
    longitude: origin.fields.coordonnees_gps[1]
  }
  const placesPromises = reduction1.map(async place => {
    const coordinates = start.longitude + ',' + start.latitude + ';' + place.longitude + ',' + place.latitude
    const response = await fetch(
      mapboxApi + coordinates + '?access_token=' + mapboxToken
    )
    const json = await response.json()
    const routes = json.routes
    if (routes.length > 0) {
      let travelTime = 0
      routes.forEach(route => {
        travelTime += route.duration
      })
      travelTime /= routes.length
      place.travelTime = travelTime
      return place
    }
  })
  const places = await Promise.all(placesPromises)
  let results = []
  for (let i = 0; i < places.length; i++) {
    if (places[i].travelTime <= input.duration * 60) {
      results.push(places[i].code);
    }
  }
  return results
}
