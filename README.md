# Ziparound (France only)

Find ZIP codes around a certain ZIP or INSEE (city ID) code, within a circle of N kilometers radius.

Forked from [guix77/ziparound](https://github.com/guix77/ziparound).

## Install

### From registry

```sh
yarn add --dev ziparound
```

### From source

```sh
yarn install
```


## Usage

If you want to refine results with travel time by car, you must get a MapBox access token, create an `.env` file and put inside:

```sh
MAPBOX_ACCESS_TOKEN=your_token
```

Download `laposte.json` data:

```sh
wget -o laposte.json https://datanova.legroupe.laposte.fr/explore/dataset/laposte_hexasmal/download/?format=json&timezone=Europe/Berlin&lang=fr
```

Then to use it:

```sh
# enter value in prompt
node index.js
# or alteratively
node index.js --code 75001 --distance 5
# or to specify a single city via INSEE
node index.js --insee --code 76540 --distance 5
```

Radius (in km) should be kept small, since the output of console.log is left to its default, 100.
